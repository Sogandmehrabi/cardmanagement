package com.mehrai.card.management.repo;

import com.mehrai.card.management.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Boolean existsByNationalCode(String nationalCode);

    Optional<User> findByExternalId(String externalId);

    Boolean existsByNationalCodeAndExternalIdNot(String nationalCode, String externalId);

    @Query("from User u where " +
            " ( :firstNamePattern is null or u.firstName like  %:firstNamePattern% ) and " +
            " ( :lastNamePattern is null or u.lastName like %:lastNamePattern% ) and " +
            " ( :nationalCodePattern is null or u.nationalCode like %:nationalCodePattern%)")
    Page<User> findByFilter(@Param("nationalCodePattern") String nationalCodePattern,
                            @Param("firstNamePattern") String firstNamePattern,
                            @Param("lastNamePattern") String lastNamePattern,
                            Pageable pageable);
}
