package com.mehrai.card.management.repo;

import com.mehrai.card.management.dto.CardSummaryDTO;
import com.mehrai.card.management.entity.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("from Transaction t where " +
            " ( :sourceCardExternalId is null or t.sourceCard.externalId = :sourceCardExternalId ) and " +
            " ( :targetCardNumber is null or t.targetCard.cardNumber = :targetCardNumber ) and " +
            " ( :fromAmount is null or t.amount >= :fromAmount ) and " +
            " ( :toAmount is null or t.amount <= :toAmount ) and " +
            " ( :from is null or t.transactionDateTime >= :from) and " +
            " ( :to is null or t.transactionDateTime <= :to ) ")
    Page<Transaction> getByFilter(@Param("sourceCardExternalId") String sourceCardExternalId,
                                  @Param("targetCardNumber") String targetCardNumber,
                                  @Param("fromAmount") Long fromAmount,
                                  @Param("toAmount") Long toAmount,
                                  @Param("from") LocalDateTime from,
                                  @Param("to") LocalDateTime to,
                                  Pageable pageable);

    @Query(" select new com.mehrai.card.management.dto.CardSummaryDTO(t.sourceCardNumber,t.transactionType,count(t) ) from Transaction t " +
            " where ( :sourceCardNumber is null or t.sourceCardNumber = :sourceCardNumber ) and " +
            " ( :from is null or t.transactionDateTime >= :from) and " +
            " ( :to is null or t.transactionDateTime <= :to ) " +
            "group by t.sourceCardNumber,t.transactionType " +
            "order by count(t) desc ")
    Page<CardSummaryDTO> getSummary(@Param("sourceCardNumber") String sourceCardNumber,
                                    @Param("from") LocalDateTime from,
                                    @Param("to") LocalDateTime to,
                                    Pageable pageable);
}
