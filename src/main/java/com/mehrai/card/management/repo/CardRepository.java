package com.mehrai.card.management.repo;

import com.mehrai.card.management.entity.Card;
import com.mehrai.card.management.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CardRepository extends JpaRepository<Card, Long> {

    Boolean existsByCardNumber(String cardNumber);

    Boolean existsByExternalId(String externalId);

    Optional<Card> findByCardNumber(String cardNumber);

    Optional<Card> getByExternalIdAndIsDeleteIsFalse(String externalId);

    Boolean existsByCardNumberAndExternalIdNot(String cardNumber, String externalId);

    @Query("from Card c where " +
            " ( :cardNumberPattern is null or c.cardNumber like  %:cardNumberPattern% ) and " +
            " ( :externalId is null or c.user.externalId = :externalId ) and c.isDelete = false ")
    Page<Card> findByFilter(@Param("cardNumberPattern") String cardNumberPattern,
                            @Param("externalId") String externalId,
                            Pageable pageable);

}
