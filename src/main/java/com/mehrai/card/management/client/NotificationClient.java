package com.mehrai.card.management.client;

import com.mehrai.card.management.exceptions.CardManagementException;

public interface NotificationClient {

    void sendNotification(String phoneNumber,String content) throws CardManagementException;
}
