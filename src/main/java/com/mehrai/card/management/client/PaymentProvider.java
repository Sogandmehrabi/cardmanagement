package com.mehrai.card.management.client;

import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.exceptions.CardManagementException;

public interface PaymentProvider {

    void payment(PaymentRequest request) throws CardManagementException;
}
