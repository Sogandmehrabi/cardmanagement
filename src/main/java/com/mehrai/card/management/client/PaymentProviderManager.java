package com.mehrai.card.management.client;

import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.exceptions.CardManagementException;

public interface PaymentProviderManager {

    void handlePayment(PaymentRequest paymentRequest) throws CardManagementException;
}
