package com.mehrai.card.management.serviceImpl;

import com.mehrai.card.management.dto.request.AddCardRequest;
import com.mehrai.card.management.dto.request.UpdateCardRequest;
import com.mehrai.card.management.dto.response.CardListResponse;
import com.mehrai.card.management.entity.Card;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.exceptions.DuplicateRecordException;
import com.mehrai.card.management.exceptions.NotFoundException;
import com.mehrai.card.management.mapper.CardDtoMapper;
import com.mehrai.card.management.repo.CardRepository;
import com.mehrai.card.management.service.CardService;
import com.mehrai.card.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;

    private final UserService userService;

    private final CardDtoMapper cardDtoMapper;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository,
                           UserService userService,
                           CardDtoMapper cardDtoMapper) {
        this.cardRepository = cardRepository;
        this.userService = userService;
        this.cardDtoMapper = cardDtoMapper;
    }

    @Override
    public void addCard(AddCardRequest request) throws CardManagementException {

        if (this.cardRepository.existsByCardNumber(request.getCardNumber())) {
            throw new DuplicateRecordException("cardNumber");
        }

        var card = new Card();
        card.setCardNumber(request.getCardNumber());
        card.setCvv2(request.getCvv2());
        card.setExpireDate(request.getExpireDate());
        card.setSecondPassword(request.getSecondPassword());
        card.setAmount(request.getAmount());

        var user = userService.findByExternalId(request.getUserExternalId());

        card.setUser(user);

        card.setIsDelete(false);
        card.setExternalId(UUID.randomUUID().toString());

        this.cardRepository.save(card);
    }

    @Override
    public void deleteCard(String externalId) throws CardManagementException {

        var card = getByExternalId(externalId);

        card.setIsDelete(true);

        this.cardRepository.save(card);

    }

    @Override
    public Card getByExternalId(String externalId) throws CardManagementException {
        return this.cardRepository.getByExternalIdAndIsDeleteIsFalse(externalId).orElseThrow(() -> new NotFoundException("Card"));
    }

    @Override
    public Card getByCardNumber(String cardNumber) throws CardManagementException {
        return this.cardRepository.findByCardNumber(cardNumber).orElseThrow(() -> new NotFoundException("Card"));
    }

    @Override
    public void save(Card card) throws CardManagementException {
        this.cardRepository.save(card);
    }

    @Override
    public void updateCard(String externalId, UpdateCardRequest request) throws CardManagementException {

        var card = getByExternalId(externalId);

        if (this.cardRepository.existsByCardNumberAndExternalIdNot(request.getCardNumber(), externalId)) {
            throw new DuplicateRecordException("CardNumber");
        }
        card.setCardNumber(request.getCardNumber());
        card.setAmount(request.getAmount());
        card.setSecondPassword(request.getSecondPassword());
        card.setExpireDate(request.getExpireDate());
        card.setCvv2(request.getCvv2());

        this.cardRepository.save(card);
    }

    @Override
    public CardListResponse getCard(String cardNumber, String userExternalId, Integer page, Integer size) throws CardManagementException {

        var dbResponseCard = this.cardRepository.findByFilter(cardNumber, userExternalId, PageRequest.of(page, size));

        var dto = dbResponseCard.
                get().
                map(cardDtoMapper::convert).
                collect(toList());


        var response = new CardListResponse();

        response.setCards(dto);
        response.setPage(dbResponseCard.getNumber());
        response.setSize(dbResponseCard.getSize());
        response.setTotal(dbResponseCard.getTotalElements());

        return response;
    }


}
