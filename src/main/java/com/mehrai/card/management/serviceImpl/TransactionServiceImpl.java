package com.mehrai.card.management.serviceImpl;

import com.mehrai.card.management.client.NotificationClient;
import com.mehrai.card.management.client.PaymentProviderManager;
import com.mehrai.card.management.dto.request.AddTransactionRequest;
import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.dto.response.CardSummaryResponse;
import com.mehrai.card.management.dto.response.TransactionListResponse;
import com.mehrai.card.management.entity.Card;
import com.mehrai.card.management.entity.Transaction;
import com.mehrai.card.management.enums.TransactionType;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.exceptions.NotAcceptableException;
import com.mehrai.card.management.mapper.TransactionDtoMapper;
import com.mehrai.card.management.repo.TransactionRepository;
import com.mehrai.card.management.service.CardService;
import com.mehrai.card.management.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static java.util.stream.Collectors.toList;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    private final CardService cardService;

    private final TransactionDtoMapper transactionDtoMapper;

    private final NotificationClient notificationClient;

    private final String messageContent = "successfully done!";

    private final PaymentProviderManager paymentProviderManager;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  CardService cardService,
                                  TransactionDtoMapper transactionDtoMapper,
                                  NotificationClient notificationClient,
                                  PaymentProviderManager paymentProviderManager) {
        this.transactionRepository = transactionRepository;
        this.cardService = cardService;
        this.transactionDtoMapper = transactionDtoMapper;
        this.notificationClient = notificationClient;
        this.paymentProviderManager = paymentProviderManager;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addTransaction(String cardExternalId, AddTransactionRequest request) throws CardManagementException {

        // fetch source card
        var sourceCard = cardService.getByExternalId(cardExternalId);

        validateRequest(sourceCard, request);

        var targetCard = cardService.getByCardNumber(request.getTargetCardNumber());

        var paymentRequest = new PaymentRequest();

        paymentRequest.setSourceCard(sourceCard.getCardNumber());
        paymentRequest.setTargetCard(targetCard.getCardNumber());
        paymentRequest.setCvv2(sourceCard.getCvv2());
        paymentRequest.setSecondPassword(sourceCard.getSecondPassword());
        paymentRequest.setExpDate(sourceCard.getExpireDate());
        paymentRequest.setAmount(request.getAmount());

        var transaction = new Transaction();

        transaction.setAmount(request.getAmount());
        transaction.setTransactionDateTime(LocalDateTime.now());
        transaction.setSourceCard(sourceCard);
        transaction.setSourceCardNumber(sourceCard.getCardNumber());
        transaction.setTargetCard(targetCard);
        try {

            paymentProviderManager.handlePayment(paymentRequest);
            transaction.setTransactionType(TransactionType.SUCCESS_FULL);
        } catch (Exception e) {
            transaction.setTransactionType(TransactionType.FAIL);
            //fetch message from third party and set failType!
            //transaction.setFailType();
        }

        transactionRepository.save(transaction);


        var amount = request.getAmount();

        var sourceAmount = sourceCard.getAmount() - amount;
        sourceCard.setAmount(sourceAmount);
        this.cardService.save(sourceCard);

        var targetAmount = targetCard.getAmount() + amount;
        targetCard.setAmount(targetAmount);
        this.cardService.save(targetCard);


        notificationClient.sendNotification(sourceCard.getUser().getPhoneNumber(), messageContent);

    }

    @Override
    public CardSummaryResponse getCardSummaryResponse(String sourceCardNumber, LocalDateTime from, LocalDateTime to, Integer page,
                                                      Integer size) {

        var dbResponse = this.transactionRepository.getSummary(sourceCardNumber, from, to, PageRequest.of(page, size));

        var response = new CardSummaryResponse();

        response.setCardSummaries(dbResponse.getContent());
        response.setPage(dbResponse.getNumber());
        response.setSize(dbResponse.getSize());
        response.setTotal(dbResponse.getTotalElements());

        return response;
    }

    @Override
    public TransactionListResponse getTransactionListResponse(String sourceCardExternalId,
                                                              String targetCardNumber,
                                                              Long fromAmount,
                                                              Long toAmount,
                                                              LocalDateTime from,
                                                              LocalDateTime to,
                                                              Integer page,
                                                              Integer size) throws CardManagementException {

        var dbResponse = this.transactionRepository.getByFilter(sourceCardExternalId, targetCardNumber, fromAmount, toAmount, from, to, PageRequest.of(page, size));

        var dto = dbResponse.
                get().
                map(transactionDtoMapper::convert).
                collect(toList());

        var response = new TransactionListResponse();

        response.setTransactions(dto);
        response.setPage(dbResponse.getNumber());
        response.setSize(dbResponse.getSize());
        response.setTotal(dbResponse.getTotalElements());

        return response;
    }

    private void validateRequest(Card card, AddTransactionRequest request) throws CardManagementException {

        // todo check over_bound

        if (card.getAmount() < request.getAmount()) {
            throw new NotAcceptableException("Card Amount can not be less than zero!");
        }

        if (!card.getCvv2().equals(request.getCcv2())) {
            throw new NotAcceptableException("CVV2 is wrong!");
        }

        if (!card.getExpireDate().equals(request.getExpireDate())) {
            throw new NotAcceptableException("expire date is wrong!");
        }

        if (!card.getSecondPassword().equals(request.getSecondPassword())) {
            throw new NotAcceptableException("second Password date is wrong!");
        }

    }
}
