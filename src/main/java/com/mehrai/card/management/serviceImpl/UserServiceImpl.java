package com.mehrai.card.management.serviceImpl;

import com.mehrai.card.management.dto.request.AddUserRequest;
import com.mehrai.card.management.dto.request.UpdateUserRequest;
import com.mehrai.card.management.dto.response.UserListResponse;
import com.mehrai.card.management.entity.User;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.exceptions.DuplicateRecordException;
import com.mehrai.card.management.exceptions.NotFoundException;
import com.mehrai.card.management.mapper.UserDtoMapper;
import com.mehrai.card.management.repo.UserRepository;
import com.mehrai.card.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserDtoMapper userDtoMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserDtoMapper userDtoMapper) {
        this.userRepository = userRepository;
        this.userDtoMapper = userDtoMapper;
    }

    @Override
    public void addUser(AddUserRequest request) throws CardManagementException {

        if (this.userRepository.existsByNationalCode(request.getNationalCode())) {
            throw new DuplicateRecordException("NotionalCode");
        }

        var user = new User();

        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setNationalCode(request.getNationalCode());
        user.setExternalId(UUID.randomUUID().toString());
        user.setPhoneNumber(request.getPhoneNumber());

        this.userRepository.save(user);
    }

    @Override
    public void updateUser(String externalId, UpdateUserRequest request) throws CardManagementException {

        var user = findByExternalId(externalId);

        if (this.userRepository.existsByNationalCodeAndExternalIdNot(request.getNationalCode(), externalId)) {
            throw new DuplicateRecordException("NotionalCode");
        }

        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setNationalCode(request.getNationalCode());
        user.setPhoneNumber(request.getPhoneNumber());

        this.userRepository.save(user);

    }

    @Override
    public UserListResponse getUser(String nationalCodePattern, String firstNamePattern, String lastNamePattern, Integer page, Integer size) throws CardManagementException {

        var dbResponse = this.userRepository.findByFilter(nationalCodePattern, firstNamePattern, lastNamePattern, PageRequest.of(page, size));

        var dto = dbResponse.
                get().
                map(userDtoMapper::convert).
                collect(toList());


        var response = new UserListResponse();

        response.setUsers(dto);
        response.setPage(dbResponse.getNumber());
        response.setSize(dbResponse.getSize());
        response.setTotal(dbResponse.getTotalElements());

        return response;
    }

    @Override
    public User findByExternalId(String externalId) throws CardManagementException {

        return this.userRepository.findByExternalId(externalId).orElseThrow(() -> new NotFoundException("User"));
    }
}
