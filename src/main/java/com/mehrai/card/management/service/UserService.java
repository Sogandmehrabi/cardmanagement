package com.mehrai.card.management.service;

import com.mehrai.card.management.dto.request.AddUserRequest;
import com.mehrai.card.management.dto.request.UpdateUserRequest;
import com.mehrai.card.management.dto.response.UserListResponse;
import com.mehrai.card.management.entity.User;
import com.mehrai.card.management.exceptions.CardManagementException;

public interface UserService {

    void addUser(AddUserRequest request) throws CardManagementException;

    void updateUser(String externalId, UpdateUserRequest request) throws CardManagementException;

    UserListResponse getUser(String nationalCodePattern, String firstNamePattern, String lastNamePattern, Integer page, Integer size) throws CardManagementException;

    User findByExternalId(String externalId) throws CardManagementException;
}
