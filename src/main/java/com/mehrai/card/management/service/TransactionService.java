package com.mehrai.card.management.service;

import com.mehrai.card.management.dto.request.AddTransactionRequest;
import com.mehrai.card.management.dto.response.CardSummaryResponse;
import com.mehrai.card.management.dto.response.TransactionListResponse;
import com.mehrai.card.management.exceptions.CardManagementException;

import java.time.LocalDateTime;

public interface TransactionService {

    void addTransaction(String cardExternalId, AddTransactionRequest request) throws CardManagementException;

    CardSummaryResponse getCardSummaryResponse(String sourceCardNumber, LocalDateTime from, LocalDateTime to, Integer page,
                                               Integer size);

    TransactionListResponse getTransactionListResponse(String sourceCardExternalId, String targetCardNumber, Long fromAmount,
                                                       Long toAmount, LocalDateTime from, LocalDateTime to, Integer page,
                                                       Integer size) throws CardManagementException;
}
