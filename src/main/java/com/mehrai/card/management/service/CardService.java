package com.mehrai.card.management.service;

import com.mehrai.card.management.dto.request.AddCardRequest;
import com.mehrai.card.management.dto.request.UpdateCardRequest;
import com.mehrai.card.management.dto.response.CardListResponse;
import com.mehrai.card.management.entity.Card;
import com.mehrai.card.management.exceptions.CardManagementException;

public interface CardService {

    void addCard(AddCardRequest request) throws CardManagementException;

    void deleteCard(String externalId) throws CardManagementException;

    Card getByExternalId(String externalId) throws CardManagementException;

    Card getByCardNumber(String cardNumber) throws CardManagementException;

    void save(Card card) throws CardManagementException;

    void updateCard(String externalId, UpdateCardRequest request) throws CardManagementException;

    CardListResponse getCard(String cardNumberPattern, String userExternalId, Integer page, Integer size) throws CardManagementException;

}
