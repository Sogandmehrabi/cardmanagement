package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.TargetUserDto;
import com.mehrai.card.management.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TargetUserDtoMapper {

    TargetUserDto convert(User user);
}
