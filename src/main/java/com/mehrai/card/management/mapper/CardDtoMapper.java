package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.CardDto;
import com.mehrai.card.management.entity.Card;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = UserDtoMapper.class)
public interface CardDtoMapper {

    @Mapping(source = "user", target = "user")
    CardDto convert(Card card);
}

