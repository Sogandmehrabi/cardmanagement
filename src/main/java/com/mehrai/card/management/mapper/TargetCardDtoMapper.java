package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.TargetCardDto;
import com.mehrai.card.management.entity.Card;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = TargetUserDtoMapper.class)
public interface TargetCardDtoMapper {

    @Mapping(source = "user", target = "user")
    TargetCardDto convert(Card card);
}

