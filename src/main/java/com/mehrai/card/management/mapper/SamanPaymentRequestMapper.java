package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.dto.request.SamanPaymentRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SamanPaymentRequestMapper {

    @Mapping(source = "sourceCard", target = "source")
    @Mapping(source = "targetCard", target = "dest")
    @Mapping(source = "secondPassword", target = "pin")
    SamanPaymentRequest convert(PaymentRequest paymentRequest);
}
