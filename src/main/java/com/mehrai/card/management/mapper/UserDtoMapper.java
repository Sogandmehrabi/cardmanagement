package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.UserDto;
import com.mehrai.card.management.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDtoMapper {

    UserDto convert(User user);
}
