package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.request.ParsianPaymentRequest;
import com.mehrai.card.management.dto.request.PaymentRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ParsianPaymentRequestMapper {


    @Mapping(source = "sourceCard",target = "source")
    @Mapping(source = "targetCard",target = "target")
    @Mapping(source = "expDate",target = "expire")
    @Mapping(source = "secondPassword",target = "pin2")
    ParsianPaymentRequest convert(PaymentRequest paymentRequest);
}
