package com.mehrai.card.management.mapper;

import com.mehrai.card.management.dto.TransactionDto;
import com.mehrai.card.management.entity.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CardDtoMapper.class, TargetCardDtoMapper.class})
public interface TransactionDtoMapper {

    @Mapping(source = "sourceCard", target = "sourceCard")
    @Mapping(source = "targetCard", target = "targetCard")
    TransactionDto convert(Transaction transaction);
}
