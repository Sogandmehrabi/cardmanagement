package com.mehrai.card.management.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Table(name = "card_tbl", indexes = {
        @Index(name = "idx_ccn", columnList = "card_number", unique = true),
        @Index(name = "idx_cei", columnList = "external_id", unique = false),
        @Index(name = "idx_cui", columnList = "user_id", unique = false)
})
@Entity
@NoArgsConstructor
@Data
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "card_number", nullable = false)
    private String cardNumber;

    @Column(name = "cvv2", nullable = false)
    private String cvv2;

    @Column(name = "expire_date", nullable = false)
    private String expireDate;

    @Column(name = "second_password", nullable = false)
    private String secondPassword;

    @Column(name = "amount", nullable = false)
    private Long amount;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "sourceCard")
    private List<Transaction> sourceTransactions;

    @OneToMany(mappedBy = "targetCard")
    private List<Transaction> targetTransactions;

    @Column(name = "is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name = "external_id", nullable = false,length = 36)
    private String externalId;

}
