package com.mehrai.card.management.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Table(name = "user_tbl",
        indexes = {
        @Index(name = "idx_unc", columnList = "national_code", unique = true),
        @Index(name = "idx_uei", columnList = "external_id", unique = false)
}
        )

@Entity
@NoArgsConstructor
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "national_code", nullable = false, unique = true)
    private String nationalCode;

    @Column(name = "external_id", nullable = false, length = 36)
    private String externalId;

    @OneToMany(mappedBy = "user")
    private List<Card> cards;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;
}
