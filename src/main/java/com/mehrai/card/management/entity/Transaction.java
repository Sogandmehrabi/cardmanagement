package com.mehrai.card.management.entity;

import com.mehrai.card.management.enums.FailType;
import com.mehrai.card.management.enums.TransactionType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "transaction_tbl", indexes = {
        @Index(name = "idx_ttt", columnList = "transaction_type", unique = false),
        @Index(name = "idx_tei", columnList = "external_id", unique = false),
        @Index(name = "idx_tdt", columnList = "transaction_date_time", unique = false),
        @Index(name = "idx_ttci", columnList = "target_card_id", unique = false),
        @Index(name = "idx_tsci", columnList = "source_card_id", unique = false),
        @Index(name = "idx_tscn", columnList = "source_card_number", unique = false)

})
@Entity
@NoArgsConstructor
@Data
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "amount", nullable = false)
    private Long amount;

    @Column(name = "transaction_date_time", nullable = false)
    private LocalDateTime transactionDateTime;

    @ManyToOne
    @JoinColumn(name = "source_card_id", nullable = false)
    private Card sourceCard;

    @Column(name = "source_card_number")
    private String sourceCardNumber;

    @ManyToOne
    @JoinColumn(name = "target_card_id", nullable = false)
    private Card targetCard;

    @Column(name = "external_id", nullable = false, length = 36)
    private String externalId;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    private TransactionType transactionType;

    @Enumerated(EnumType.STRING)
    @Column(name = "fail_type")
    private FailType failType;
}
