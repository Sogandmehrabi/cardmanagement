package com.mehrai.card.management.enums;

public enum FailType {
    NOT_ENOUGH_CREDIT,
    NO_RESPONSE_FROM_SERVER,
    OVER_BOUND
}
