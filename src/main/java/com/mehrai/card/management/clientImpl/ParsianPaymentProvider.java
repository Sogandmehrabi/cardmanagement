package com.mehrai.card.management.clientImpl;

import com.mehrai.card.management.client.PaymentProvider;
import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.mapper.ParsianPaymentRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("parsian")
public class ParsianPaymentProvider implements PaymentProvider {

    private final ParsianPaymentRequestMapper parsianPaymentRequestMapper;

    @Autowired
    public ParsianPaymentProvider(ParsianPaymentRequestMapper parsianPaymentRequestMapper) {
        this.parsianPaymentRequestMapper = parsianPaymentRequestMapper;
    }

    @Override
    public void payment(PaymentRequest request) throws CardManagementException {


        var parsianPaymentRequest = parsianPaymentRequestMapper.convert(request);

        //call parsian server
    }
}
