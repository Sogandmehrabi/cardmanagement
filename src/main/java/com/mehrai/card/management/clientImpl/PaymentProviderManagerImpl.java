package com.mehrai.card.management.clientImpl;

import com.mehrai.card.management.client.PaymentProvider;
import com.mehrai.card.management.client.PaymentProviderManager;
import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.exceptions.CardManagementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PaymentProviderManagerImpl implements PaymentProviderManager {

    private final PaymentProvider samanPaymentProvider;

    private final PaymentProvider parsianPaymentProvider;

    @Autowired
    public PaymentProviderManagerImpl(@Qualifier("saman") PaymentProvider samanPaymentProvider,
                                      @Qualifier("parsian") PaymentProvider parsianPaymentProvider) {

        this.samanPaymentProvider = samanPaymentProvider;
        this.parsianPaymentProvider = parsianPaymentProvider;
    }

    @Override
    public void handlePayment(PaymentRequest paymentRequest) throws CardManagementException {

        if (paymentRequest.getSourceCard().startsWith("6037")) {

            samanPaymentProvider.payment(paymentRequest);

        } else {
            parsianPaymentProvider.payment(paymentRequest);
        }

    }
}
