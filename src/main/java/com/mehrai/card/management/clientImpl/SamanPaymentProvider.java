package com.mehrai.card.management.clientImpl;

import com.mehrai.card.management.client.PaymentProvider;
import com.mehrai.card.management.dto.request.PaymentRequest;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.mapper.SamanPaymentRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("saman")
public class SamanPaymentProvider implements PaymentProvider {

    private final SamanPaymentRequestMapper samanPaymentRequestMapper;

    @Autowired
    public SamanPaymentProvider(SamanPaymentRequestMapper samanPaymentRequestMapper) {
        this.samanPaymentRequestMapper = samanPaymentRequestMapper;
    }

    @Override
    public void payment(PaymentRequest request) throws CardManagementException {

        var samanPaymentRequest = samanPaymentRequestMapper.convert(request);

        // call saman service
    }
}
