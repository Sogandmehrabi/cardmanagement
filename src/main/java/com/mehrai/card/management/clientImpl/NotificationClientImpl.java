package com.mehrai.card.management.clientImpl;

import com.mehrai.card.management.client.NotificationClient;
import com.mehrai.card.management.exceptions.CardManagementException;
import org.springframework.stereotype.Service;

@Service
public class NotificationClientImpl implements NotificationClient {

    @Override
    public void sendNotification(String phoneNumber, String content) throws CardManagementException {

        //call notification service
    }
}
