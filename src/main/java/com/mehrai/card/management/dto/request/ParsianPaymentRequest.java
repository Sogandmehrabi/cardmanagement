package com.mehrai.card.management.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ParsianPaymentRequest {

    private String source;

    private String target;

    private String cvv2;

    private String expire;

    private String pin2;

    private Long amount;
}
