package com.mehrai.card.management.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaymentRequest {

    private String sourceCard;

    private String targetCard;

    private String cvv2;

    private String secondPassword;

    private String expDate;

    private Long amount;
}
