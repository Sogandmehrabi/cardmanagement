package com.mehrai.card.management.dto.response;

import com.mehrai.card.management.dto.CardSummaryDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CardSummaryResponse extends PaginationResponse {

    private List<CardSummaryDTO> cardSummaries;
}
