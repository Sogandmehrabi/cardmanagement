package com.mehrai.card.management.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class AddTransactionRequest {

    @NotEmpty
    private String ccv2;

    @NotEmpty
    private String expireDate;

    @NotEmpty
    private String secondPassword;

    @NotNull
    @Min(1)
    private Long amount;

    @NotEmpty
    private String targetCardNumber;
}
