package com.mehrai.card.management.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaginationResponse {

    protected Integer size;

    protected Integer page;

    protected Long total;
}
