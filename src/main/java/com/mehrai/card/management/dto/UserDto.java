package com.mehrai.card.management.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto {

    private String firstName;

    private String lastName;

    private String nationalCode;

    private String externalId;

    private String phoneNumber;
}
