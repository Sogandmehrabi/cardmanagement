package com.mehrai.card.management.dto;

import com.mehrai.card.management.enums.FailType;
import com.mehrai.card.management.enums.TransactionType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@Data
public class TransactionDto {

    private Long amount;

    private LocalDateTime transactionDateTime;

    private CardDto sourceCard;

    private TargetCardDto targetCard;

    private String externalId;

    private TransactionType transactionType;

    private FailType failType;
}
