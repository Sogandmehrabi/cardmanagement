package com.mehrai.card.management.dto.response;

import com.mehrai.card.management.dto.CardDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CardListResponse extends PaginationResponse {

    List<CardDto> cards;
}
