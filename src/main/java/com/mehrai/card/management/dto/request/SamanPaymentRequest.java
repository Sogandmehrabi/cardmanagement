package com.mehrai.card.management.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SamanPaymentRequest {

    private String source;

    private String dest;

    private String cvv2;

    private String expDate;

    private String pin;

    private Long amount;
}
