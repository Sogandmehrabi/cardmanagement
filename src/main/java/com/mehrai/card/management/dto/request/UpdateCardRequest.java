package com.mehrai.card.management.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class UpdateCardRequest {

    @NotEmpty
    private String cardNumber;

    @NotEmpty
    private String cvv2;

    @NotEmpty
    private String expireDate;

    @NotEmpty
    private String secondPassword;

    @Min(0)
    private Long amount;
}
