package com.mehrai.card.management.dto;

import com.mehrai.card.management.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardSummaryDTO {

    private String cardNumber;

    private TransactionType transactionType;

    private Long count;
}
