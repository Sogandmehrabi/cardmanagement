package com.mehrai.card.management.dto.response;

import com.mehrai.card.management.dto.TransactionDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class TransactionListResponse extends PaginationResponse {

    private List<TransactionDto> transactions;
}
