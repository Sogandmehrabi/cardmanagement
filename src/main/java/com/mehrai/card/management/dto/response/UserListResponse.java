package com.mehrai.card.management.dto.response;

import com.mehrai.card.management.dto.UserDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class UserListResponse extends PaginationResponse {

    private List<UserDto> users;
}
