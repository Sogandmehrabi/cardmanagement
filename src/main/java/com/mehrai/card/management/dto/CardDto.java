package com.mehrai.card.management.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CardDto {

    private String cardNumber;

    private String cvv2;

    private String expireDate;

    private String secondPassword;

    private Long amount;

    private UserDto user;

    private String externalId;

}



