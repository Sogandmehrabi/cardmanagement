package com.mehrai.card.management.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TargetUserDto {

    private String firstName;

    private String lastName;
}
