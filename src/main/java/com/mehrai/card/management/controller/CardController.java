package com.mehrai.card.management.controller;

import com.mehrai.card.management.dto.request.AddCardRequest;
import com.mehrai.card.management.dto.request.UpdateCardRequest;
import com.mehrai.card.management.dto.response.CardListResponse;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api/v1/cards")
@Validated
public class CardController {

    private final CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.OK)
    public void addCard(@Valid @RequestBody AddCardRequest request) throws CardManagementException {
        this.cardService.addCard(request);
    }

    @PutMapping("/{externalId}")
    @ResponseStatus(value = HttpStatus.OK)
    public void updateCard(@PathVariable String externalId, @Valid @RequestBody UpdateCardRequest request) throws CardManagementException {
        this.cardService.updateCard(externalId, request);
    }

    @DeleteMapping("/{externalId}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteCard(@PathVariable String externalId) throws CardManagementException {
        this.cardService.deleteCard(externalId);
    }

    @GetMapping
    public ResponseEntity<CardListResponse> getCard(@RequestParam(required = false) String cardNumberPattern,
                                                    @RequestParam(required = false) String userExternalId,
                                                    @RequestParam(required = false, defaultValue = "0") @Min(0) Integer page,
                                                    @RequestParam(required = false, defaultValue = "20") @Max(100) @Min(1) Integer size) throws CardManagementException {

        var response = this.cardService.getCard(cardNumberPattern, userExternalId, page, size);

        return ResponseEntity.ok(response);
    }
}
