package com.mehrai.card.management.controller;

import com.mehrai.card.management.dto.response.CardManagementExceptionDTO;
import com.mehrai.card.management.exceptions.CardManagementException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class CardManagementExceptionController {


    private static final Function<FieldError, String> FIELD_ERROR_PROCESSOR = fieldError -> fieldError.getField()
            .concat(" has invalid value: ").concat(fieldError.getDefaultMessage());

    @ExceptionHandler(CardManagementException.class)
    public ResponseEntity<CardManagementExceptionDTO> handelOrderManagerException(HttpServletRequest request, CardManagementException e) {
        log.warn("caught exception during serving request. request: '{}' , error : '{}'", requestInfo(request), e.getMsg());
        return ResponseEntity.
                status(e.getHttpStatus()).
                body(new CardManagementExceptionDTO(e.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<CardManagementExceptionDTO> echoResponseMessage(MethodArgumentNotValidException exception) {
        log.warn("received bad request", exception);
        String message = exception.getBindingResult().getFieldErrors().stream()
                .map(FIELD_ERROR_PROCESSOR)
                .collect(Collectors.joining(";"));
        return ResponseEntity
                .badRequest()
                .body(new CardManagementExceptionDTO(message));
    }

    @ExceptionHandler({ConstraintViolationException.class, HttpMessageNotReadableException.class})
    public ResponseEntity<CardManagementExceptionDTO> echoResponseMessage(Exception exception) {
        log.debug("received bad request", exception);
        String message = exception.getMessage();
        return ResponseEntity
                .badRequest()
                .body(new CardManagementExceptionDTO(message));
    }

    //for inputs of type LocalDate
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<CardManagementExceptionDTO> echoResponseMessage(MethodArgumentTypeMismatchException exception) {
        log.debug("received bad request", exception);
        String message = "value for '".concat(exception.getParameter().getParameterName()).concat("' is not valid: ").concat(exception.getMostSpecificCause().getMessage());
        return ResponseEntity
                .badRequest()
                .body(new CardManagementExceptionDTO(message));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<CardManagementExceptionDTO> echoResponseMessage(MissingServletRequestParameterException exception) {
        log.debug("received bad request", exception);
        String message = "value for request-param '".concat(exception.getParameterName()).concat("' is missing");
        return ResponseEntity
                .badRequest()
                .body(new CardManagementExceptionDTO(message));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<CardManagementExceptionDTO> handleUnknownException(Exception exception) {
        String errorReference = UUID.randomUUID().toString();
        log.warn("Caught exception, reference: '{}'", errorReference, exception);
        String message = "Internal OrderManager error. You can trace this in logs using reference: '".concat(errorReference).concat("'");
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new CardManagementExceptionDTO(message));
    }

    private String requestInfo(HttpServletRequest request) {


        StringBuilder requestInfoBuilder = new StringBuilder();

        requestInfoBuilder.append("path : ");
        requestInfoBuilder.append(request.getPathTranslated());
        requestInfoBuilder.append(", Method : ");
        requestInfoBuilder.append(request.getMethod());

        return requestInfoBuilder.toString();

    }
}
