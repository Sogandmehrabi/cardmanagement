package com.mehrai.card.management.controller;

import com.mehrai.card.management.dto.request.AddTransactionRequest;
import com.mehrai.card.management.dto.response.CardSummaryResponse;
import com.mehrai.card.management.dto.response.TransactionListResponse;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/v1/transactions")
@Validated
public class TransactionController {

    private final TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/{cardExternalId}")
    @ResponseStatus(value = HttpStatus.OK)
    public void addTransaction(@PathVariable String cardExternalId, @Valid @RequestBody AddTransactionRequest request) throws CardManagementException {

        transactionService.addTransaction(cardExternalId, request);
    }

    @GetMapping
    public ResponseEntity<TransactionListResponse> getTransactionsList(@RequestParam(required = false) String sourceCardExternalId,
                                                                       @RequestParam(required = false) String targetCardNumber,
                                                                       @RequestParam(required = false) Long fromAmount,
                                                                       @RequestParam(required = false) Long toAmount,
                                                                       @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                                       @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                                       @RequestParam(required = false, defaultValue = "0") @Min(0) Integer page,
                                                                       @RequestParam(required = false, defaultValue = "20") @Max(100) @Min(1) Integer size) throws CardManagementException {

        var response = this.transactionService.getTransactionListResponse(sourceCardExternalId, targetCardNumber, fromAmount, toAmount, from, to, page, size);

        return ResponseEntity.ok(response);

    }

    @GetMapping("/summaries")
    public ResponseEntity<CardSummaryResponse> getSummaries(@RequestParam(required = false) String sourceCardNumber,
                                                            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                            @RequestParam(required = false, defaultValue = "0") @Min(0) Integer page,
                                                            @RequestParam(required = false, defaultValue = "20") @Max(100) @Min(1) Integer size) {

        var response = this.transactionService.getCardSummaryResponse(sourceCardNumber, from, to, page, size);

        return ResponseEntity.ok(response);
    }
}
