package com.mehrai.card.management.controller;

import com.mehrai.card.management.dto.request.AddUserRequest;
import com.mehrai.card.management.dto.request.UpdateUserRequest;
import com.mehrai.card.management.dto.response.UserListResponse;
import com.mehrai.card.management.exceptions.CardManagementException;
import com.mehrai.card.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api/v1/users")
@Validated
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping
    @ResponseStatus(value = HttpStatus.OK)
    public void addUser(@Valid @RequestBody AddUserRequest request) throws CardManagementException {
        userService.addUser(request);
    }
//void esposse status mikhad
    @PutMapping(value = "/{externalId}")
    @ResponseStatus(value = HttpStatus.OK)
    public void updateUser(@PathVariable String externalId, @Valid @RequestBody UpdateUserRequest request) throws CardManagementException {
        userService.updateUser(externalId, request);
    }

    @GetMapping
    //dar badane ok mishavad
    public ResponseEntity<UserListResponse> getUserList(@RequestParam(required = false) String nationalCodePattern,
                                                        @RequestParam(required = false) String firstNamePattern,
                                                        @RequestParam(required = false) String lastNamePattern,
                                                        @RequestParam(required = false, defaultValue = "0") @Min(0) Integer page,
                                                        @RequestParam(required = false, defaultValue = "20") @Max(100) @Min(1) Integer size) throws CardManagementException {

        var response = userService.getUser(nationalCodePattern, firstNamePattern, lastNamePattern, page, size);

        return ResponseEntity.ok(response);
    }
}
