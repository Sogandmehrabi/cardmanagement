package com.mehrai.card.management.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class NotAcceptableException extends CardManagementException {

    public NotAcceptableException(String msg) {
        super(msg, HttpStatus.BAD_REQUEST);
    }
}
