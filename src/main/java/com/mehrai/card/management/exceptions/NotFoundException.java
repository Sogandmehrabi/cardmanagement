package com.mehrai.card.management.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class NotFoundException extends CardManagementException {

    public NotFoundException(String item) {
        super(item.concat(" Not Found!"), HttpStatus.NOT_FOUND);
    }
}
