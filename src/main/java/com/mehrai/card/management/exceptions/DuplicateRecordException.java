package com.mehrai.card.management.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class DuplicateRecordException extends CardManagementException {


    public DuplicateRecordException(String item) {
        super("Duplicate Record by ".concat(item), HttpStatus.BAD_REQUEST);
    }
}
