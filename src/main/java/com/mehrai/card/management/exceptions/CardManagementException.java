package com.mehrai.card.management.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class CardManagementException extends Exception {

    private final String msg;

    private final HttpStatus httpStatus;

    public CardManagementException(String msg, HttpStatus httpStatus) {
        this.msg = msg;
        this.httpStatus = httpStatus;
    }
}
